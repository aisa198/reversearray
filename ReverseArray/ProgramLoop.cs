﻿using System;
using System.Linq;

namespace ReverseArray
{
    internal class ProgramLoop
    {
        public void Run()
        {
            var sizeOfArray = GetInt();
            var array = GetArray();
            var reversedArray = ReverseArray(array);
            PrintArray(reversedArray);
            Console.ReadLine();
        }

        private string[] ReverseArray(string[] array)
        {
            if (array != null)
            {
                var start = 0;
                var end = array.Length - 1;
                string temp;
                while (start < end)
                {
                    temp = array[start];
                    array[start] = array[end];
                    array[end] = temp;
                    start++;
                    end--;
                }
            }
            return array;
        }
        private void PrintArray(string[] array)
        {
            if(array != null)
            {
                foreach (var item in array)
                    Console.Write(item + " ");
            }
        }

        private int GetInt()
        {
            string input;
            int number;
            do
            {
                input = Console.ReadLine();
            } while (!Int32.TryParse(input, out number) || number < 1);
            return number;
        }

        private string[] GetArray()
        {
            var array = Console.ReadLine();
            try
            {
                return array.Split(' ').ToArray(); //.Select(x => Convert.ToInt32(x, new CultureInfo("pl-PL"))) - if it must be an int array
            }
            catch(FormatException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
